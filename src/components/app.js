import React from 'react';
import { View } from 'react-native';

import Header from './header';
import AlbumList from './AlbumList';

const App = () => (
  <View style={{ flex: 1 }}>
    <Header text="Albums" />
    <AlbumList />
  </View>
);

export default App;
